<?php

namespace xiaolian\card;

use Exception;

class Xlcardpay
{

    private $host;
    private $gatewayUrl;
    private $appId;
    private $rsaPrivateKey;
    private $alipayrsaPublicKey;
    private $apiVersion;
    private $signType;
    private $postCharset;
    private $format;
    private $encryptKey;
    /**
     * @var string
     */
    private $payUrl;
    /**
     * @var string
     */
    private $queryUrl;
    /**
     * @var string
     */
    private $MemberListUrl;
    /**
     * @var mixed|string
     */
    private $shop_account;
    /**
     * @var string
     */
    private $refundUrl;
    /**
     * @var string
     */
    private $GetMoneyLog;

    /** 支付方式初始化
     * Xlcardpay constructor.
     * @param string $server_host
     * @param string $app_key
     * @param string $priKey
     * @param string $pub_key
     * @param string $encrypt_key
     * @param string $shop_account
     */
    function __construct($server_host, $app_key, $priKey, $pub_key, $encrypt_key, $shop_account)
    {
        $this->host = $server_host;

        $this->payUrl = $this->host  . '/card/paymentByCard';
        $this->refundUrl = $this->host  . '/card/OrderRefund';
        $this->queryUrl = $this->host  . '/card/queryCard';
        $this->MemberListUrl = $this->host  . '/card/GetMemberList';
        $this->GetMoneyLog = $this->host.'/card/GetMoneyLog';

        $this->appId = $app_key;
        $this->shop_account = $shop_account;

        $this->apiVersion = '1.0';
        $this->signType = 'RSA2';
        $this->postCharset = 'utf-8';
        $this->format = 'json';
        $this->rsaPrivateKey = $priKey;
        $this->alipayrsaPublicKey = $pub_key;
        $this->encryptKey = $encrypt_key;
    }

    /**
     * @param string $order_no 订单编号
     * @param string $account 账号|物理卡ID
     * @param int $money 支付金额(分)
     * @param string $account_type ""=>账号|"physical"=>物理卡ID
     * @return array
     * @throws Exception
     */
    public function gdccard_pay($order_no,$account,$money,$account_type = "")
    {
        $aop = new AopClient();
        $aop->gatewayUrl = $this->payUrl;
        $aop->appId = $this->appId;
        $aop->rsaPrivateKey = $this->rsaPrivateKey;
        $aop->alipayrsaPublicKey = $this->alipayrsaPublicKey;
        $aop->apiVersion = $this->apiVersion;
        $aop->signType = $this->signType;
        $aop->postCharset = $this->postCharset;
        $aop->format = $this->format;
        $aop->encryptKey = $this->encryptKey;
        $parm = array(
            'type' => 'third_order_pay',
            'code' => $order_no,
            'accountType' => $account_type,
            'account' => $account,
            'money' => $money,
            'shop_account' => $this->shop_account,
            //'shop_account'=>$this->shop_code,
            'msg' => '接口交易',
        );
        $request = new XlPaymentRequest();

        $request->setBizContent(json_encode($parm));

        $result = $aop->execute($request);
        if (isset($result['data']) && $result['code'] == 0) {
            $result_data = $result['data'];
            if ($result_data['pay_status'] == 1) {
                return [
                    'errcode' => '0',
                    'retcode' => '0',
                    'out_id' => $result_data['flow_code'],
                    'errmsg' => $result_data['msg'],
                    'order_no' => $order_no,
                    'mercacc' => '001',
                    'tranamt' => $money,
                    'accountType' => $account_type,
                    'account' => $account
                ];
            }
            if (in_array($result_data['pay_status'], array(-1))) {
                return array(
                    'errcode' => '60050',
                    'retcode' => '60050',
                    'out_id' => $result_data['flow_code'],
                    'errmsg' => $result_data['msg'],
                    'order_no' => $order_no,
                    'mercacc' => '001',
                    'tranamt' => $money,
                    'accountType' => $account_type,
                    'account' => $account
                );
            }

            if (in_array($result_data['pay_status'], array(2, -1, 0, -2))) {
                return array('errcode' => '500', 'retcode' => '500',
                    'out_id' => $result_data['flow_code'], 'errmsg' => $result_data['msg'],
                    'order_no' => $order_no, 'mercacc' => '001', 'tranamt' => $money,
                    'accountType' => $account_type,'account' => $account);
            }

        }

//        Log::error(json_encode($result));
        return array(
            'retcode' => -1,
            'msg' => isset($result["msg"])?$result["msg"]:'服务器繁忙',
            'order_no' => $order_no,
            'mercacc' => '001',
            'tranamt' => $money,
            'accountType' => $account_type,
            'account' => $account
        );
    }

    /**
     * @param int|string $order_no 订单编号
     * @param float $refund_amount 退款金额(元)
     * @return array
     * @throws Exception
     */
    public function OrderRefund($order_no, $refund_amount)
    {
        $aop = new AopClient();
        $aop->gatewayUrl = $this->refundUrl;
        $aop->appId = $this->appId;
        $aop->rsaPrivateKey = $this->rsaPrivateKey;
        $aop->alipayrsaPublicKey = $this->alipayrsaPublicKey;
        $aop->apiVersion = $this->apiVersion;
        $aop->signType = $this->signType;
        $aop->postCharset = $this->postCharset;
        $aop->format = $this->format;
        $aop->encryptKey = $this->encryptKey;
        $parm = array(
            'orderCode' => $order_no,
            'money' => $refund_amount, //退款金额
//            'trade_money' => (int)bcmul($trade_money, 100, 0),
            //'shop_account'=>$this->shop_code,
            'msg' => '接口退款',
            "comId" => 1
        );
        $request = new XlOrderRefundRequest();

        $request->setBizContent(json_encode($parm));

        $result = $aop->execute($request);
        if (isset($result['code']) && $result['code'] == 0) {
            return array(
                "retcode" => 0,
                "errcode" => 0,
                "msg" => $result["msg"] ?: "退款成功"
            );
        }
//        Log::error(json_encode($result));
        return array(
            'retcode' => -1,
            'errcode' => $result['code'] ?: -1,
//            'out_id' => $order_no,
            'errmsg' => $result["msg"] ?: '服务器繁忙',
            'order_no' => $order_no,
            'mercacc' => '001',
//            'tranamt' => $trade_money,
            'money' => $refund_amount,
        );
    }

    /**
     * @param int|string $code  账户|物理ID|手机号
     * @param int $type 查询类型 1,2:账户,3:物理ID,4:手机号
     * @return array
     * @throws Exception
     */
    public function query_card($code, $type = 1)
    {
        $aop = new AopClient();
        $aop->gatewayUrl = $this->queryUrl;
        $aop->appId = $this->appId;
        $aop->rsaPrivateKey = $this->rsaPrivateKey;
        $aop->alipayrsaPublicKey = $this->alipayrsaPublicKey;
        $aop->apiVersion = $this->apiVersion;
        $aop->signType = $this->signType;
        $aop->postCharset = $this->postCharset;
        $aop->format = $this->format;
        $aop->encryptKey = $this->encryptKey;
        $request = new XlQueryCardRequest();
        $parm = array(
            'Type' => $type,
            'code' => $code,
        );
        $request->setBizContent(json_encode($parm));
        $info = $aop->execute($request);
        if (isset($info['data']) && $info['code'] == 0) {
            $card['cardtype'] = isset($info['data']['card_type']) ? $info['data']['card_type'] : 0;
            $card['physical_id'] = isset($info['data']['physical_id']) ? $info['data']['physical_id'] : 0;
            $card['account'] = isset($info['data']['account']) ? $info['data']['account'] : 0;
            $card['name'] = isset($info['data']['real_name']) ? $info['data']['real_name'] : 0;
            $card['phone'] = isset($info['data']['phone_number']) ? $info['data']['phone_number'] : 0;
//            $card['cert'] = '';
//            $card['expdate'] = '';
            $card['card_balance'] = isset($info['data']['card_balance']) ? $info['data']['card_balance'] : 0;
            $card['sno'] = isset($info['data']['sno']) ? $info['data']['sno'] : 0;
            $card['cards'] = isset($info['data']['cards']) ? $info['data']['cards'] : [];

            $result['retcode'] = '0';
            $result['msg'] = 'SUCCESS';
            $result['info'] = $card;
            return $result;
        } else {
            $re['retcode'] = '-1';
            $re['msg'] = isset($info["msg"])?$info["msg"]:"查询失败";
            return $re;
        }


    }

    /** 查询卡信息更新人员信息
     * @param int|string $updated_at 根据更新时间搜索 int|2023-10-11 12:00:00|空字符串
     * @param string $keyword 模糊搜索
     * @param int $page
     * @param int $pageSize
     * @return false|mixed|\SimpleXMLElement
     * @throws Exception
     */
    public function GetMemberList($updated_at = "",$keyword="",$page=1,$pageSize=100)
    {
        $aop = new AopClient();
        $aop->gatewayUrl = $this->MemberListUrl;
        $aop->appId = $this->appId;
        $aop->rsaPrivateKey = $this->rsaPrivateKey;
        $aop->alipayrsaPublicKey = $this->alipayrsaPublicKey;
        $aop->apiVersion = $this->apiVersion;
        $aop->signType = $this->signType;
        $aop->postCharset = $this->postCharset;
        $aop->format = $this->format;
        $aop->encryptKey = $this->encryptKey;
        $request = new XlQueryCardRequest();
        if (is_int($updated_at)){
            $updated_at = date('Y-m-d H:i:s', time() - 86400 * $updated_at);//更新时间选择前一天
        }elseif($updated_at!==""){
            $updated_at = date("Y-m-d H:i:s",strtotime($updated_at));
        }
        $parm = array(
            'updated_at' => $updated_at,
            'keyword' => $keyword,
            'page'=>$page,
            'pageSize'=>$pageSize
        );
        $request->setBizContent(json_encode($parm));
        $result = $aop->execute($request);
        return $result;
    }

    /**
     * @param string $account 账户
     * @param string $type member=>member_id|mobile=>手机号|account=>会员卡号|physical=>实体卡号
     * @param int $page
     * @param int $pageSize
     * @return false|mixed
     * @throws Exception
     */
    public function GetMoneyLog($account, $type="member",$page=1, $pageSize=10){//查询卡信息更新人员信息
        $aop = new AopClient();
        $aop->gatewayUrl = $this->GetMoneyLog;
        $aop->appId = $this->appId;
        $aop->rsaPrivateKey = $this->rsaPrivateKey;
        $aop->alipayrsaPublicKey = $this->alipayrsaPublicKey;
        $aop->apiVersion = $this->apiVersion;
        $aop->signType = $this->signType;
        $aop->postCharset = $this->postCharset;
        $aop->format = $this->format;
        $aop->encryptKey = $this->encryptKey;
        $request = new XlGetMoneyLogRequest();
        // $updated_at='2021-03-30';
        $parm = array(
            'type'=>$type,
            'page'=>$page,
            'pageSize'=>$pageSize
        );
        $request->setBizContent(json_encode($parm));
        $result = $aop->execute($request);
        return $result;
    }
}